//Leonardo Dulanto
//ldulanto
//PA1
//1/20/24
//-----------------------------------------------------------------------------
// List.c
// Implementation file for List ADT
//-----------------------------------------------------------------------------
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

#include "List.h"

// structs --------------------------------------------------------------------
// private Node type
typedef struct NodeObj* Node;

// private NodeObj type
// NodeObj contain fields for the data 
// wo Node references (previous and next Nodes)
typedef struct NodeObj{
   int data;
   Node previous;
   Node next;
} NodeObj;

// private ListObj type
// ListObj have fields of type Node referring to the front, back and cursor.
// ListObj have int fields for the length of a List, and the index of the cursor element.
typedef struct ListObj{
   Node front;
   Node back;
   Node cursor;
   int length;
   int index;
} ListObj;


// Constructors-Destructors ---------------------------------------------------
// createNode()
// Returns reference to new Node object. Initializes next and data fields.
Node createNode(int data){
   Node N = malloc(sizeof(NodeObj));
   N->data = data;
   N->previous = NULL;
   N->next = NULL;
   return(N);
}

// releaseNode()
// Frees heap memory pointed to by *pN, sets *pN to NULL.
void releaseNode(Node* pN){
   if( pN!=NULL && *pN!=NULL ){
      free(*pN);
      *pN = NULL;
      pN = NULL;
   }
}

// newEmptyList()
// Returns reference to new empty List object.
List newEmptyList(){
   List L;
   L = (List)malloc(sizeof(ListObj));
   L->front = L->back = L->cursor = NULL;
   L->length = 0;
   L->index = -1;
   return(L);
}

// releaseList()
// Frees all heap memory associated with List *pL, and sets *pL to NULL.
void releaseList(List* pL){
   if(pL!=NULL && *pL!=NULL) { 
      resetList(*pL);
      free(*pL);
      *pL = NULL;
      pL = NULL;
   }
}

// Access functions -----------------------------------------------------------
int listLength(List L){
   return(L->length);
}

int listIndex(List L){
   return(L->index);
}

int listFront(List L){
   return(L->front->data);
}

int listBack(List L){
   return(L->back->data);
}

int listGet(List L){
   return(L->cursor->data);
}

bool listEquals(List A, List B){
   if(A->length != B->length){
      return(false);
   }
   if(A->length == 0 && B->length == 0){
      return(true);
   }
   moveToStart(A);
   moveToStart(B);
   while( listIndex(A)>=0 && listIndex(B)>=0 ){
      int a = listGet(A);
      int b = listGet(B);
      if(a != b){
         return(false);
      }
      moveCursorNext(A);
      moveCursorNext(B);
   }
   return(true);
}

// Manipulation procedures ----------------------------------------------------

void resetList(List L){
   if(L->length == 0){
      return;
   }
   Node temp_node;
   Node node = L->front;
   while(node != L->back){
      temp_node = node->next;
      releaseNode(&node);
      node = temp_node;
   }
   free(L->back);
   L->front = L->back = L->cursor = NULL;
   L->length = 0;
   L->index = -1;
}

void listSet(List L, int x){
   L->cursor->data = x;
}

void moveToStart(List L){
   if(L->length > 0){
      L->index = 0;
      L->cursor = L->front;
   }
}

void moveToEnd(List L){
   if(L->length > 0){
      L->index = L->length - 1;
      L->cursor = L->back;
   }
}

void moveCursorPrev(List L){
   if(L->cursor != NULL){
      if(L->cursor != L->front){
         L->index -= 1;
         L->cursor = L->cursor->previous;
      }else{
         L->cursor = NULL;
         L->index = -1;
      }
   }
}

void moveCursorNext(List L){
   if(L->cursor != NULL){
      if(L->cursor != L->back){
         L->index += 1;
         L->cursor = L->cursor->next;
      }else{
         L->cursor = NULL;
         L->index = -1;
      }
   }
}

void listPrepend(List L, int x){
   Node X = createNode(x);
   if(L->length > 0){
      X->next = L->front;
      L->front->previous = X;
      L->front = X;
      if(L->index != -1){
         L->index += 1;
      }
   }else{
      L->front = L->back = X;
   }
   L->length += 1;
}

void listAppend(List L, int x){
   Node X = createNode(x);
   if(L->length > 0){
      X->previous = L->back;
      L->back->next = X;
      L->back = X;
   }else{
      L->front = L->back = X;
   }
   L->length += 1;
}

void listInsertBefore(List L, int x){
   if(L->cursor == L->front){
      listPrepend(L, x);
      return;
   }
   Node X = createNode(x);
   Node node_before_cursor = L->cursor->previous;
   X->next = L->cursor;
   X->previous = node_before_cursor;
   L->cursor->previous = X;
   node_before_cursor->next = X;
   L->length += 1;
   L->index += 1;
}

void listInsertAfter(List L, int x){
   if(L->cursor == L->back){
      listAppend(L, x);
      return;
   }
   Node X = createNode(x);
   Node node_after_cursor = L->cursor->next;
   X->previous = L->cursor;
   X->next = node_after_cursor;
   L->cursor->next = X;
   node_after_cursor->previous = X;
   L->length += 1;
}

void removeFirst(List L){
   if(L->length == 1){
      resetList(L);
      return;
   }
   if(listIndex(L) >= 0){
      L->index -= 1;
   }
   if(L->cursor == L->front){
      L->cursor = NULL;
   }
   Node oldFront = L->front;
   L->front = oldFront->next;
   releaseNode(&oldFront);
   L->length -= 1;
}

void removeLast(List L){
   if(L->length == 1){
      resetList(L);
      return;
   }
   if(L->cursor == L->back){
      L->cursor = NULL;
      L->index = -1;
   }
   Node back = L->back;
   L->back = NULL;
   L->back = back->previous;
   releaseNode(&back);
   L->length -= 1;
}

void listDelete(List L){
   if(L->cursor == L->front){
      removeFirst(L);
      return;
   }
   if(L->cursor == L->back){
      removeLast(L);
      return;
   }
   Node cursor = L->cursor;
   Node node_before_cursor = cursor->previous;
   Node node_after_cursor = cursor->next;
   if(node_before_cursor) node_before_cursor->next = node_after_cursor;
   if(node_after_cursor) node_after_cursor->previous = node_before_cursor;
   L->cursor = NULL;
   L->index = -1;
   releaseNode(&cursor);
   L->length -= 1;
}

List duplicateList(List L){
   List new_L = newEmptyList();
   int index_L = listIndex(L); // Record origin index of L
   moveToStart(L);
   while(listIndex(L) >= 0){
      int x = listGet(L);
      listAppend(new_L, x);
      moveCursorNext(L);
   }
   moveToStart(L);  // Recover the index of L to origin index
   while (listIndex(L) != index_L){
      moveCursorNext(L);
   }
   return new_L;
}

List combineLists(List A, List B){
   List L = newEmptyList();
   moveToStart(A);
   while(listIndex(A) >= 0){
      int x = listGet(A);
      listAppend(L, x);
      moveCursorNext(A);
   }
   moveToStart(B);
   while(listIndex(B) >= 0){
      int x = listGet(B);
      listAppend(B, x);
      moveCursorNext(B);
   }
   return(L);
}

void outputList(FILE* output, List L){
   moveToStart(L);
   while(listIndex(L) >= 0){
      int x = listGet(L);
      fprintf(output, "%d ", x);
      moveCursorNext(L);
   }
   // fprintf(output, "\n");
}
