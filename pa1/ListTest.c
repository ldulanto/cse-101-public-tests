//Leonardo Dulanto
//ldulanto
//PA1
//1/20/24
/****************************************************************************************
*  ListTest.c
*  Test client for List ADT
*****************************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include"List.h"

int main(int argc, char* argv[]){

   // two empty lists, A and B, to test our List ADT
   List A = newEmptyList();
   List B = newEmptyList();
   List C = NULL; // for duplicating one of our lists
   int i;

   // populate lists with some data
   // append numbers 1-30 to A, and prepend them to B
   for(i = 1; i <= 30; i++){
      listAppend(A, i);
      listPrepend(B, i);
   }

   // print list to check there data.
   outputList(stdout, A); 
   printf("\n");
   outputList(stdout, B); 
   printf("\n");

   // traverse list A from start to finish
   moveToStart(A);
   while(listIndex(A) >= 0){
      printf("%d ", listGet(A));
      moveCursorNext(A);
   }
   printf("\n");

   // traverse List from end to start
   moveToEnd(B);
   while(listIndex(B) >= 0){
      printf("%d ", listGet(B));
      moveCursorPrev(B);
   }
   printf("\n");

   // duplicate list A into list C and compare these lists
   C = duplicateList(A);
   printf("%s\n", listEquals(A, B) ? "true" : "false"); // Compare A and B
   printf("%s\n", listEquals(B, C) ? "true" : "false"); // Compare B and C
   printf("%s\n", listEquals(C, A) ? "true" : "false"); // Compare C and A

   // modify list A a bit for testing purposes
   // We'll insert -1 before the 6th element and -2 after the 15th element
   // And then, delete the 10th element
   moveToStart(A);
   for(i = 0; i < 5; i++) moveCursorNext(A); // Move to index 5
   listInsertBefore(A, -1);                  // Insert -1 before index 6
   for(i = 0; i < 9; i++) moveCursorNext(A); // Move to index 15
   listInsertAfter(A, -2);                   // Insert -2 after index 15
   for(i = 0; i < 5; i++) moveCursorPrev(A); // Move back to index 10
   listDelete(A);                            // Delete element at index 10
   outputList(stdout, A);
   printf("\n");

   // check length of A, reset it, and then check its length again
   printf("%d\n", listLength(A)); // Length of A before reset
   resetList(A);                  // Resetting list A
   printf("%d\n", listLength(A)); // Length of A after reset

   // Finally, let's clean up and free all the lists we created
   releaseList(&A);
   releaseList(&B);
   releaseList(&C);

   return(0);
}
