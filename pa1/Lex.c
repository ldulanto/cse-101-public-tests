//Leonardo Dulanto
//ldulanto
//PA1
//1/20/24
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<string.h>

#include"List.h"

#define MAX_LEN 100

// duplicateString creates a copy of the given string in heap memory
char *duplicateString(const char *str){
   int n = strlen(str) + 1;
   char *dup = malloc(n);
   if(dup){
      strcpy(dup, str); 
   }
   return dup;
}

int main(int argc, char * argv[]){
   int line_count;
   FILE *in, *out;
   char line[MAX_LEN];

   // Verifying the command line arguments
   if( argc != 3 ){
      fprintf(stderr, "Usage: %s <input file> <output file>\n", argv[0]);
      exit(1);
   }

   // Open input and output files 
   in = fopen(argv[1], "r");
   if( in == NULL ){
      fprintf(stderr, "Unable to open file %s for reading\n", argv[1]);
      exit(1);
   }

   out = fopen(argv[2], "w");
   if( out == NULL ){
      fprintf(stderr, "Unable to open file %s for writing\n", argv[2]);
      exit(1);
   }

   // Counting the number of lines in the input file
   line_count = 0;
   while( fgets(line, MAX_LEN, in) != NULL )  {
      line_count++;
   }

   // Reading lines from the file
   rewind(in);
   char** str_list = (char**) calloc(sizeof(char*), line_count + 1);
   int str_list_idx = 0;
   while( fgets(line, MAX_LEN, in) != NULL )  {
      str_list[str_list_idx] = duplicateString(line); 
      str_list_idx++;
   }

   // Sorting the lines using a List ADT
   List L = newEmptyList();
   listAppend(L, 0);
   moveToStart(L);
   for(int i = 1; i < line_count; i++){
      moveToStart(L);
      while(listIndex(L) >= 0){
         if(strcmp(str_list[listGet(L)], str_list[i]) > 0){
            break;
         }
         moveCursorNext(L);
      }
      if(listIndex(L) < 0){
         listAppend(L, i);
      }else{
         listInsertBefore(L, i);
      }
   }
 
   // Writing sorted lines to the output file
   moveToStart(L);
   while(listIndex(L) >= 0){
      int x = listGet(L);
      fprintf(out, "%s", str_list[x]);
      moveCursorNext(L);
   }

   // Releasing allocated memory
   releaseList(&L);
   for(int i = 0; i < line_count + 1; i++){
      free(str_list[i]);
   }
   free(str_list);

   // Closing the files
   fclose(in);
   fclose(out);

   return 0;
}
