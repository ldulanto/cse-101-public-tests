//Leonardo Dulanto
//ldulanto
//PA1
//1/20/24
//-----------------------------------------------------------------------------
// List.h
// Header file for List ADT
//-----------------------------------------------------------------------------
#ifndef LIST_H_INCLUDE_
#define LIST_H_INCLUDE_

#include<stdio.h>
#include<stdbool.h>

// Exported Type --------------------------------------------------------------
typedef struct ListObj* List;

// Constructors-Destructors ---------------------------------------------------

// newEmptyList creates and returns a new empty List.
List newEmptyList(void);      

// releaseList frees all heap memory associated with *pL, and sets *pL to NULL.
void releaseList(List* pL);    

// Access functions -----------------------------------------------------------

// listLength returns the number of elements in L.
int listLength(List L);

// listIndex returns the position of the cursor element if defined, -1 otherwise.
int listIndex(List L);

// listFront returns the first element of L. Pre: listLength()>0
int listFront(List L);

// listBack returns the last element of L. Pre: listLength()>0
int listBack(List L);       

// listGet returns the current cursor element of L. Pre: listLength()>0, listIndex()>=0
int listGet(List L);   

// listEquals returns true if Lists A and B are in the same state, and false otherwise.     
bool listEquals(List A, List B);        

// Manipulation procedures ----------------------------------------------------

// resetList resets L to its original empty state.
void resetList(List L); 

// listSet overwrites the cursor element’s data with x. Pre: listLength()>0, listIndex()>=0
void listSet(List L, int x);

// moveToStart sets the cursor under the first element if L is non-empty, does nothing otherwise.
void moveToStart(List L);     

// moveToEnd sets the cursor under the last element if L is non-empty, does nothing otherwise.
void moveToEnd(List L);  

// moveCursorPrev moves the cursor one step toward the start of L if possible.
void moveCursorPrev(List L);  

// moveCursorNext moves the cursor one step toward the end of L if possible.
void moveCursorNext(List L);      

// listPrepend inserts a new element before the first element of L if L is non-empty.
void listPrepend(List L, int x); 

// listAppend inserts a new element after the last element of L if L is non-empty.
void listAppend(List L, int x);     

// listInsertBefore inserts a new element before the cursor. List Must be: listLength()>0, listIndex()>=0
void listInsertBefore(List L, int x);   

// listInsertAfter inserts a new element after the cursor. List Must be: listLength()>0, listIndex()>=0
void listInsertAfter(List L, int x);    

// removeFirst deletes the first element. List Must be: listLength()>0
void removeFirst(List L);  

// removeLast deletes the last element. List Must be: listLength()>0
void removeLast(List L);    

// listDelete deletes the cursor element, making the cursor undefined. List Must be: listLength()>0, listIndex()>=0
void listDelete(List L);        

// duplicateList returns a new List representing the same integer sequence as L.
List duplicateList(List L);      

// combineLists returns a new List which is the concatenation of A and B.
List combineLists(List A, List B);    

// outputList prints the List content to the provided output file.
void outputList(FILE* output, List L);       

#endif
