//Leonardo Dulanto
//ldulanto
//PA1
//1/20/24
# CSE 101 Programming Assignment 1

## Overview

This repository contains the C programming assignment focusing on the implementation and testing of a List Abstract Data Type (ADT) and a lexical analysis program (Lex). The List ADT is utilized by the Lex program to sort lines from an input file and write the sorted lines to an output file.

## Files

- `List.h`: Header file for the List ADT.
- `List.c`: Implementation of the List ADT.
- `Lex.c`: Lexical analysis program that uses the List ADT for sorting.
- `ListTest.c`: Test client for the List ADT.
- `Makefile`: A set of directives used by the `make` build automation tool to compile the project.

## Compilation and Execution

To compile the programs, we use a Makefile. Here are the key commands:

- `make`: Compiles the `Lex` program.
- `make ListTest`: Compiles the `ListTest` program.
- `make clean`: Removes all compiled binaries and object files.
- `make checkListTest`: Checks the `ListTest` program for memory errors using `valgrind`.
- `make checkLex`: Checks the `Lex` program for memory errors using `valgrind`.

After compilation, you can run the programs as follows:

- Run `ListTest`: `./ListTest`
- Run `Lex`: `./Lex input.txt output.txt` (Replace `input.txt` and `output.txt` with your file names.)

## Functionality

### List ADT

The List ADT implements a doubly linked list that allows for various operations such as insertion, deletion, and traversal.

### Lex Program

The Lex program reads lines from an input file, uses the List ADT to sort these lines, and then writes the sorted lines to an output file.

## Testing

The `ListTest.c` program contains various test cases to ensure the List ADT functions correctly. It includes tests for edge cases and typical use cases.

## Memory Error Checking

Using `valgrind`, we can check for memory leaks and errors in our programs to ensure robust memory management.

## Author

Leonardo Dulanto.

## Acknowledgements

Thanks to the CSE 101 staff for providing guidance and specifications for this assignment.

---

Feel free to explore and test the implementation! Your suggestions and contributions to improve this project are highly welcomed.
